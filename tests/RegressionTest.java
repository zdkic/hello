import org.junit.*;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.internal.TextListener;
import org.junit.runner.notification.Failure;
import static org.junit.Assert.*;
import org.junit.Test;

public class RegressionTest {
    private Hello hello;

    @Before
    public void setUp() {
        hello = new Hello();
    }

    @After
    public void tearDown() {
        hello = null;
    }

    @Test
    public void testCase1() {
        boolean ret = hello.isValid("()");
        assertEquals(true, ret);
    }

    @Test
    public void testCase2() {
        boolean ret = hello.isValid("()[]{}");
        assertEquals(true, ret);
    }

    @Test
    public void testCase3() {
        boolean ret = hello.isValid("(]");
        assertEquals(false, ret);
    }

    @Test
    public void testCase4() {
        boolean ret = hello.isValid("([)]");
        assertEquals(false, ret);
    }

    @Test
    public void testCase5() {
        boolean ret = hello.isValid("{[]}");
        assertEquals(true, ret);
    }

    @Test
    public void testCase6() {
        boolean ret = hello.isValid("(((((())))))");
        assertEquals(true, ret);
    }

    @Test
    public void testCase7() {
        boolean ret = hello.isValid("()()()()");
        assertEquals(true, ret);
    }

    @Test
    public void testCase8() {
        boolean ret = hello.isValid("(((((((()");
        assertEquals(false, ret);
    }

    @Test
    public void testCase9() {
        boolean ret = hello.isValid("((()(())))");
        assertEquals(true, ret);
    }

    @Test
    public void testCase10() {
        boolean ret = hello.isValid("((()(()))))");
        assertEquals(false, ret);
    }

    public static void main(String... args) {
        JUnitCore junit = new JUnitCore();
        junit.addListener(new TextListener(System.out));

        Result result = junit.run(RegressionTest.class);
        System.out.println("Finished. Result: Failures: " +
                            result.getFailureCount() + ". Ignored: " +
                            result.getIgnoreCount() + ". Tests run: " +
                            result.getRunCount() + ". Time: " +
                            result.getRunTime() + "ms.");
    }
}