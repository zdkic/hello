#!/usr/bin/sh
mkdir -p libs classes
wget -O libs/junit-4.13.jar https://search.maven.org/remotecontent?filepath=junit/junit/4.13/junit-4.13.jar
wget -O libs/hamcrest-2.2.jar https://search.maven.org/remotecontent?filepath=org/hamcrest/hamcrest/2.2/hamcrest-2.2.jar
javac hello.java -d classes
javac -cp .:libs/junit-4.13.jar:classes tests/SanityTest.java -d classes
javac -cp .:libs/junit-4.13.jar:classes tests/RegressionTest.java -d classes