import java.util.Stack;

class Hello {
    public boolean isValid(String s) {
        int n = s.length();
        Stack<Character> st = new Stack<>();
        for (int i = 0; i < n; i++) {
            char curr = s.charAt(i);
            if (curr == '[' || curr == '{' || curr == '(') {
                st.push(curr);
            } else {
                if (st.isEmpty()) {
                    return false;
                } else if (st.peek() == '[' && curr == ']' || st.peek() == '(' && curr == ')'
                        || st.peek() == '{' && curr == '}') {
                    st.pop();
                } else {
                    return false;
                }
            }
        }
        return st.isEmpty();
    }
}